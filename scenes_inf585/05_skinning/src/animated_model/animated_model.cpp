#include "animated_model.hpp"


using namespace cgp;



void animated_model_structure::skinning_lbs()
{
    // ************************************************************** //
	// TO DO: Compute the Linear Blend Skinning (LBS) deformation
	// ...
	// ************************************************************** //
    //
    // Help:
    //     - The function should update the values of rigged_mesh.mesh_deformed.position based on the skeleton and bind pose (rigged_mesh.mesh_bind_pose.position)
    //     - Once the computation is working on the position, you may also update the normals
    //     - The skinning weights are available via: rigged_mesh.skinning_weight
    //       They are stored per vertex and per joint: float weight_ij = rigged_mesh.skinning_weight[vertex_i][joint_j];
    //       
    //     - Given a mat4 M representing a rigid transformation (with rotation and translation only), you can compute efficiently its inverse using the syntax
    //        mat4 M_inversed = M.inverse_assuming_rigid_transform();
    //     - Consider a mat4 M representing a projective (or affine, or rigid) transformation, and a vec3 p. We call q the transformation of p by M.
    //         - If p represents a 3D point, then q can be expressed as
    //             vec3 q = vec3( M * vec4(p,1.0f) ); or similarily vec3 q = M.transform_position(p);
    //         - If p represents a 3D vector, then q can be expressed as
    //             vec3 q = vec3( M * vec4(p,0.0f) ); or similarily vec3 q = M.transform_vector(p);
    //  

    // Example of looping over the positions of the mesh
    int N_vertex = rigged_mesh.mesh_bind_pose.position.size();
    for(int k_vertex=0; k_vertex<N_vertex; ++k_vertex) {
        vec3 const& position_in_bind_pose = rigged_mesh.mesh_bind_pose.position[k_vertex]; // The "initial/bind pose" position p0
        vec3 const& normal_in_bind_pose = rigged_mesh.mesh_bind_pose.normal[k_vertex];     // The "initial/bind pose" normal n0
        vec3& position_to_be_deformed = rigged_mesh.mesh_deformed.position[k_vertex];      // The position to be deformed by LBS
        vec3& normal_to_be_deformed = rigged_mesh.mesh_deformed.normal[k_vertex];         // The normal to be deformed by LBS

        position_to_be_deformed = vec3(0,0,0);
        normal_to_be_deformed = vec3(0,0,0);
        for(int joint=0; joint<skeleton.size(); joint++) {
            float weight_ij = rigged_mesh.skinning_weight[k_vertex][joint];
            auto Mj = skeleton.joint_matrix_global[joint];
            auto Mj0inv = skeleton.joint_matrix_global_bind_pose[joint].inverse_assuming_rigid_transform();
            auto T = Mj * Mj0inv;
            position_to_be_deformed += weight_ij * T.transform_position(position_in_bind_pose);
            normal_to_be_deformed += weight_ij * T.transform_vector(normal_in_bind_pose);
        }

        // // Do some computation ...
        // position_to_be_deformed = position_in_bind_pose;   // to be changed
        // normal_to_be_deformed  = normal_in_bind_pose; // to be changed
    }

}

struct DualQuat {
    quaternion q0;
    quaternion qe;

    DualQuat() {}
    DualQuat(quaternion q0, quaternion qe): q0(q0), qe(qe) {}

    void fromQuatAndTranslation(const quaternion & q, const vec3 & t) {
        this->q0 = q;
        this->qe = 0.5f * quaternion(t.x, t.y, t.z, 0) * q;
    }

    void toQuatAndTranslation(quaternion & q, vec3 & t) {
        q = this->q0;
        quaternion qt = 2.f * this->qe * conjugate(this->q0);
        t = {qt.x, qt.y, qt.z};
    }

    void normalize() {
        q0 = (1.f/norm(q0))*q0;
        qe = (1.f/norm(q0))*qe;
    }

    DualQuat & operator+=(const DualQuat & q) {
        this->q0 += q.q0;
        this->qe += q.qe;
        return *this;
    }
};

DualQuat operator+(const DualQuat & Q1, const DualQuat & Q2) {
    return {
        Q1.q0 + Q2.q0,
        Q1.qe + Q2.qe
    };
}
DualQuat operator-(const DualQuat & Q1, const DualQuat & Q2) {
    return {
        Q1.q0 - Q2.q0,
        Q1.qe - Q2.qe
    };
}
DualQuat operator*(float f, const DualQuat & Q) {
    return {
        Q.q0*f,
        Q.qe*f
    };
}
DualQuat operator*(const DualQuat & Q1, const DualQuat & Q2) {
    return {
        Q1.q0 * Q2.q0,
        Q1.qe*Q2.q0 + Q1.q0*Q2.qe
    };
}
DualQuat operator/(const DualQuat & Q1, const DualQuat & Q2) {
    return {
        Q1.q0 / Q2.q0,
        (Q1.qe*Q2.q0 - Q1.q0*Q2.qe) / (Q2.q0*Q2.q0)
    };
}

void animated_model_structure::skinning_dqs()
{
    // ************************************************************** //
	// TO DO: Compute Dual Quaternion Skinning (DQS) deformation
	// ...
	// ************************************************************** //
    //
    // Help:
    //     - Given a mat4 representing a rigid transformation, the following syntax allows to access the rotation and translation part:
    //         affine_rt a = affine_rt::from_matrix({mat4});
    //         rotation_transform rot = a.rotation
    //         vec3 translation = a.translation
    //     - The quaternion of a rotation_transform can be accessed via {rotation_transform}.get_quaternion();
    //     - The structure quaternion is a specialized type derived from a vec4. You can access to its .x .y .z .w component similarily to a vec4.
    //     

    int N_vertex = rigged_mesh.mesh_bind_pose.position.size();
    for(int k_vertex=0; k_vertex<N_vertex; ++k_vertex) {
        vec3 const& position_in_bind_pose = rigged_mesh.mesh_bind_pose.position[k_vertex]; // The "initial/bind pose" position p0
        vec3 const& normal_in_bind_pose = rigged_mesh.mesh_bind_pose.normal[k_vertex];     // The "initial/bind pose" normal n0
        vec3& position_to_be_deformed = rigged_mesh.mesh_deformed.position[k_vertex];      // The position to be deformed by LBS
        vec3& normal_to_be_deformed = rigged_mesh.mesh_deformed.normal[k_vertex];         // The normal to be deformed by LBS

        cgp::numarray<DualQuat> qs;
        qs.resize(skeleton.size());
        for(int joint=0; joint<skeleton.size(); joint++) {
            float weight_ij = rigged_mesh.skinning_weight[k_vertex][joint];
            auto Mj = skeleton.joint_matrix_global[joint];
            auto Mj0inv = skeleton.joint_matrix_global_bind_pose[joint].inverse_assuming_rigid_transform();
            auto T = Mj * Mj0inv;

            DualQuat qj;
            qj.fromQuatAndTranslation(
                cgp::affine_rt::from_matrix(T).rotation.get_quaternion(),
                cgp::affine_rt::from_matrix(T).translation
            );

            qs[joint] = weight_ij * qj;
        }

        quaternion quat;
        vec3 transl;
        DualQuat q = sum(qs);
        q.normalize();
        q.toQuatAndTranslation(quat, transl);

        cgp::affine_rt transform;
        transform.rotation.from_quaternion(quat);
        transform.translation = transl;

        position_to_be_deformed = transform.matrix().transform_position(position_in_bind_pose);
        normal_to_be_deformed = transform.matrix().transform_vector(normal_in_bind_pose);
    }
}
